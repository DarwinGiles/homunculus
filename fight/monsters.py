import random
from fight import attacks

class Monster(object):
    def __init__(self, name, hp):
        self.name = name
        self.max_hp = hp
        self.current_hp = hp
        self.state = "alive"
        self.attack_time = 10
        self.attacks = []
    
    def get_info(self):
        info = self.name
        info += f"\nHP: {self.current_hp}/{self.max_hp}"
        if not self.check_life():
            info = f"The {self.name} is dead! You won!"
        return info

    def deal_damage(self, damage):
        self.current_hp -= damage
        self.check_life()
    
    def heal(self, health):
        self.current_hp += health
        self.check_life()

    def check_life(self):
        if self.current_hp <= 0:
            self.state = "dead"
            self.current_hp = 0
            return False
        elif self.current_hp > self.max_hp:
            self.current_hp = self.max_hp
        return True

    def random_attack(self):
        return self.attacks[random.randint(0, len(self.attacks) - 1)]

class Goblin(Monster):
    def __init__(self):
        super().__init__("Goblin", 10)
        self.attacks.append(attacks.Attack("The Goblin stabs {} with its sword, dealing {} damage.", 1, 4))
        self.attacks.append(attacks.Attack("The Goblin tosses a bomb at {}, blowing them up for {} damage.", 1, 8))
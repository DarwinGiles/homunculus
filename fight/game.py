from base_game import Game
import discord
import re
import fight.monsters
import random
import asyncio

FIGHT_REGEX = re.compile(r"!f (\w+)")

class Fight_Game(Game):
    def __init__(self, starting_playername):
        super().__init__("Fight", FIGHT_REGEX)
        self.current_monster = fight.monsters.Goblin()
        self.participating_players = {starting_playername: 10}
        self.defeated_players = []
        self.attack_loop = False
    
    async def action(self, message):
        if message.author.name in self.defeated_players:
            await self.message_channel.send(
                f"{message.author.name}, you have already been defeated and can no longer participate in the fight.")
            return
        elif message.author.name not in self.participating_players.keys():
            await self.add_user(message.author.name)

        attack = FIGHT_REGEX.search(message.content).group(1)
        if attack == "Sword":
            damage = random.randint(1, 8)
            await self.message_channel.send(f"You swing your sword at the {self.current_monster.name}, dealing {damage}(1d8) damage.")
            self.current_monster.deal_damage(damage)
        elif attack == "Bow":
            damage = random.randint(1, 8)
            await self.message_channel.send(f"You fire an arrow at the {self.current_monster.name}, dealing {damage}(1d8) damage.")
            self.current_monster.deal_damage(damage)
        else:
            damage = random.randint(1, 6)
            await self.message_channel.send(f"You {attack} at the {self.current_monster.name}, dealing {damage}(1d6) damage.")
            self.current_monster.deal_damage(damage)
        
        await self.message_channel.send(self.current_monster.get_info())
        if not self.current_monster.check_life():
            self.game_state = "end"
    
    async def get_status(self):
        await self.message_channel.send(f"An evil {self.current_monster.name} blocks your path. You must fight it off!")
        await self.message_channel.send(self.current_monster.get_info())
        if not self.attack_loop:
            await self.monster_cooldown()

    async def monster_cooldown(self):
        if self.game_state != "end":   
            self.attack_loop = True
            await asyncio.sleep(self.current_monster.attack_time)
            await self.message_channel.send(self.monster_attack())
            await self.monster_cooldown()
    
    async def add_user(self, username):
        await self.message_channel.send(f"{username} has entered the fray!")
        self.participating_players[username] = 10
    
    def monster_attack(self):
        if self.participating_players == {}:
            return f"The {self.current_monster.name} stands around waiting for a foe."
        target_player = random.choice(list(self.participating_players.items()))[0]
        attack = self.current_monster.random_attack()
        damage = random.randint(attack.min_damage, attack.max_damage)
        self.participating_players[target_player] -= damage
        attack_text = attack.attack_string.format(target_player, damage)
        attack_text += f"\n{target_player} has {self.participating_players[target_player]} HP remaining."
        if self.participating_players[target_player] <= 0:
            attack_text += f"\n{target_player} has been defeated. They will no longer be able to participate in the battle."
            self.participating_players.pop(target_player)
            self.defeated_players.append(target_player)
            print(self.defeated_players)
        return attack_text

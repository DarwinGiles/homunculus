"""Logic for playing the game "High Low" 
The bot picks a random number and users are asked to guess what it is.
Users are then told whether their guess was higher or lower than the goal."""

import random
import re
from base_game import Game
import discord
import asyncio

NUMGUESS_REGEX = re.compile(r"guess (\d\d?)")

class High_Low(Game):
    def __init__(self):
        super().__init__("High Low", NUMGUESS_REGEX)
        self.current_guesses = 0
        self.max_guesses = 6
        self.answer = random.randint(1, 20)
    
    async def get_status(self):
        status = "I'm thinking of a number between 1 and 20."
        status += f"\nYou have {self.max_guesses} guesses."
        if self.current_guesses > 0:
            status += f"\nYou've already made {self.current_guesses} guesses."
        await self.message_channel.send(status)

    async def action(self, message):
        guess_num = NUMGUESS_REGEX.search(message.content).group(1)
        self.current_guesses += 1
        response = f"Guess number {self.current_guesses}: {guess_num}"
        if int(guess_num) == self.answer:
            response += "\nThat's correct! You Win!"
            self.game_state = "end"
        elif self.current_guesses >= self.max_guesses:
            response += "\nThat's not correct. That was your final guess. You lose. :("
            self.game_state = "end"
        elif int(guess_num) > self.answer:
            response += "\nYou're too high!"
        elif int(guess_num) < self.answer:
            response += "\nYou're too low!"

        await self.message_channel.send(response)

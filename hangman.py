import random
import re
from base_game import Game
import asyncio
import os
import discord

HANGMAN_REGEX = re.compile(r"!hang (\w+)")

HANGMAN_PICS_FOLDER = os.path.join(".", "hangman_pics")

HANGMAN_PICS = []

for x in range(7):
    HANGMAN_PICS.append(os.path.join(HANGMAN_PICS_FOLDER, f"hangman_{x}.jpg"))

def get_word_list():
    f = open(os.path.join(".", "hangman.md"))
    text = f.read()
    words = text.split("\n")
    return words

def get_word():
    words = get_word_list()
    return words[random.randint(0, len(words) - 1)]

class Hangman(Game):
    def __init__(self):
        super().__init__("Hangman", HANGMAN_REGEX)
        self.my_word = get_word()
        self.wrong_guesses = 0
        self.used_letters = []
        self.max_wrong_guesses = 6
        self.answer_string = "?" * len(self.my_word)
        self.replace_char(" ")
        self.wrong_guesses = 0
        self.used_letters = []
    
    def replace_char(self, char):
        miss = True
        i = 0
        new_answer = []
        self.used_letters.append(char)
        for letter in self.answer_string:
            new_answer.append(letter)
        for letter in self.my_word:
            if letter == char:
                new_answer[i] = letter
                miss = False

            self.answer_string = "".join(new_answer)
            i += 1
        if miss:
            self.wrong_guesses += 1
    
    async def get_status(self):
        await self.message_channel.send(file=discord.File(HANGMAN_PICS[self.wrong_guesses]))
        status = "You are playing Hangman!\n"
        status += " ".join(self.answer_string)
        status += "\nLetters used so far:\n"
        status += "--".join(self.used_letters)
        if self.wrong_guesses >= self.max_wrong_guesses:
            status += "\nYou have died, GAME OVER\n"
            status += f"The correct answer was {self.my_word}"
            self.game_state = "end"
        elif self.answer_string == self.my_word:
            status += "\nYou Won! Congratulations!"
            self.game_state = "end"
        await self.message_channel.send(status)

    async def action(self, message):
        answer = HANGMAN_REGEX.search(message.content).group(1)
        for char in answer:
            self.replace_char(char)
            if self.wrong_guesses >= self.max_wrong_guesses:
                break
        await self.get_status()

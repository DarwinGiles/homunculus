from urllib import request
from bs4 import BeautifulSoup


URL_BASE = "https://www.youtube.com/results?search_query={}"

def getVideo(search):
    formatted_search = format_search(search)
    query = URL_BASE.format(formatted_search)
    response = request.urlopen(query)
    soup = BeautifulSoup(response, "html.parser")
    for vid in soup.find_all(attrs={'class': 'yt-uix-tile-link'}):
        if not "googleadservices" in vid['href'] and vid['href'].startswith("/watch"):
            return "https://www.youtube.com" + vid['href']

def format_search(search):
    formatted = search.split(" ")
    formatted = "+".join(formatted)
    return formatted
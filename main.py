import discord
import asyncio
import random
import re
import credentials
import hangman
import highLow
import fight.game
import youtube
import test

client = discord.Client()

dice_regex = re.compile(r"(\d+)d(\d+)")
current_game = None
game_regex = None
youtube_regex = re.compile(r"^YT")

def roll_dice(number, size):
    total = 0
    rolls = []
    for i in range(number):
        result = random.randint(1, size)
        rolls.append(result)
        total += result
    return total, rolls

@client.event
async def on_ready():
    print("Logged in as")
    print(client.user.name)
    print(client.user.id)
    print("------")

@client.event
async def on_message(message):
    global current_game
    global game_regex

    if message.content.startswith("!"):
        command = message.content[1:]

        if command == 'highlow':
            await start_game(message, highLow.High_Low())
        elif command == 'hangman':
            await start_game(message, hangman.Hangman())
        elif command == 'fight':
            await start_game(message, fight.game.Fight_Game(message.author.name))
        elif command == 'gamestatus' and current_game:
            await current_game.get_status()
        elif current_game and game_regex.search(message.content):
            guess = game_regex.search(message.content)
            await current_game.action(message)
            if current_game.game_state == "end":
                end_game()
        elif command == "help":
            await message.author.send("Whoops I deleted the help info. Will fix it later.")
            await client.delete_message(message)
        elif youtube_regex.search(command) and len(command) > 3:
            search = command[4:]
            await message.channel.send(youtube.getVideo(search))
        elif dice_regex.search(command):
            for roll in dice_regex.findall(command):
                result, rolls = roll_dice(int(roll[0]), int(roll[1]))
                await message.channel.send("*Dice Rolling Noises*")
                if int(roll[0]) > 1:
                    await message.channel.send(f"Your total is {result}.  {rolls}")
                else:
                    await message.channel.send(f"Your total is {result}.")

def end_game():
    """Removes any active games from the bot's search stuff."""
    global current_game
    global game_regex
    print(f"Finished playing {current_game.game_name}")
    current_game = None
    game_regex = None

async def start_game(message, new_game):
    global current_game
    global game_regex
    global client
    if current_game:
        await message.channel("A game is already in progress. Please wait until it is over before starting another.")
        return
    current_game = new_game
    current_game.discord_client = client
    current_game.message_channel = message.channel
    game_regex = current_game.game_regex
    print(f"Began playing {current_game.game_name}.")
    await current_game.get_status()

client.run(credentials.TOKEN)
